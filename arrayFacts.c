#include <stdio.h>

int main() {

    // Here is an array;
    double prices[] = {3.99, 4.99, 5.99, 6.99};

    // @TODO: 2. Using the array, prove that "the name of the array" is the same as a pointer to the first element in the array.
    // @TODO:        - HINT: Can you prove that they point to the same location in memory?


    // @TODO: 3. Prove that prices[3] means "return the value that is 3 positions away from the starting element of prices"
    // @TODO:       - HINT: Use pointer math?



    // @TODO: 4. What is the output of *(prices + 2)?
    // @TODO        - What is a simpler way of writing *(prices + 2)?

    
    return 0;
}

