#include <stdio.h>

int main() {

    // Here is a constant pointer

    double price1 = 8.99;
    double price2 = 13.99;
    double * const priceMemoryLocation;

    // @TODO: 1. Print the address of price 1
    printf("The address of price1 is: 0x_______\n");

    // @TODO: 2. Print the address of price 2
    printf("The address of price2 is: 0x_______\n");

    // @TODO: 3. Assign priceMemoryLocation to the address of price1


    // @TODO: 4. Compare the value stored in priceMemoryLocation with the address of price1
    printf("Price 1 Location: 0x_____;  PriceMemoryLocation: 0x_______\n");

    // @TODO: 5. Change priceMemoryLocation to point to the address of price2
    // @TODO:        - does it work?

    printf("Price 1 Location: 0x_____;  PriceMemoryLocation: 0x_______\n");

    return 0;
}

