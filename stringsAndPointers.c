#include <stdio.h>

int main() {

    // here is a string constant:
    char alphabet[27] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    // Part 1 - Strings and Pointers
    // @TODO: Where in memory is the "A" in "ABCD..." stored?
    // @TODO: Where in memory is "ABCDEFGHIJKLMNOPQRSTUVWXYZ" stored?
    // @TODO: Where in memory is the pointer to "ABCDEFHGIJKLMNOPQRSTUVWXYZ" stored?

    printf("Memory location of A:                    0x%p\n", alphabet);
    printf("Memory location of ABCDEF..XYZ:          0x%p\n", alphabet);
    printf("Memory location of alphabet variable:    0x%p\n", alphabet);


    // ----------------------------
    // Part 2 - Printing Immutable Strings
    // ----------------------------

    // This prints out the entire alphabet
    printf("Result1: %s\n", alphabet);

    // @TODO: 1. Using the alphabet variable and a single printf statement, output all letters from B-Z.
    printf("Result2: ______\n", alphabet);

    // @TODO: 2. Using the alphabet variable and a single print statement, output all letters from T-Z.
    printf("Result3: ______\n", alphabet);


    // ----------------------------
    // Part 2 - Printing Mutable Strings
    // ----------------------------
    // here is a mutable string:
    char * city = "BARCELONA";

    // @TODO: Output the city
    printf("City1: %s\n", city);

    // @TODO: Output the memory location where the city is stored
    printf("City2: 0x%p\n", city);

    // @TODO: Using [] notation, output the first letter of the city.
    // @TODO:   - Repeat using pointer arithmetic
    printf("City3: %c\n", city);

    // @TODO: Using [] notation, output "RCELONA"
    // @TODO:   - Repeat using pointer arithmetic
    printf("City4: %s\n", city);


    return 0;

}

