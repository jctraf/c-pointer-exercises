#include <stdio.h>

void applySalesTax(double p);

int main() {

    double p = 3.79;
    printf("What is p? %.2f\n", p);

    applySalesTax(p);

    // After running the function, p has not changed value (it is still the original value)
    // @TODO: Modify the applySalesTax() function so that p actually includes a 13% sales tax
    // HINT: You are not allowed to change the return type of the function
    printf("After running the function, what is p? %.2f\n", p);
    return 0;
}

// @TODO: Make your modifications here
void applySalesTax(double price) {
    price = price * 1.13;
    printf("--Price with tax:  %.2f\n", price);
}

