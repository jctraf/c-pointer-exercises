#include <stdio.h>

typedef struct {
    int levelNumber;            // level number
    double numEnemies;             // number of enemies spawned on the level
    double timeAllowed;         // time to complete level (in seconds)
} GameLevel;

int main() {
    GameLevel level1;
    level1.levelNumber = 1;
    level1.numEnemies = 25;
    level1.timeAllowed = 45;

    // @TODO: Where in memory is level1 stored?
    // @TODO: Where in memory is level1's timeAllowed property stored?


    // @TODO: Create a pointer to level1.

    // @TODO: Using the pointer, output the address of level1's timeAllowed property.

    // @TODO: Using the pointer, update level1 by increasing the number of enemies in level1 by 30%

    return 0;

}

